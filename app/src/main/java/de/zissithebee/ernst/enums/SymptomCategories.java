package de.zissithebee.ernst.enums;

public enum SymptomCategories {
    SKIN, RESPIRATORY_SYTEM, GASTROINTESTINAL, CIRCULATION, HEADACHE, OTHER
}
