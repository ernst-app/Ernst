package de.zissithebee.ernst.enums;

public enum EntryCategories {
    DISH, SYMPTOM, MEDICINE, NOTE
}
