package de.zissithebee.ernst.enums;

public enum KeysForMedicine {
    NAME, DOSE, CATEGORY, CATEGORY_IMAGE, ID_MEDICINE
}
