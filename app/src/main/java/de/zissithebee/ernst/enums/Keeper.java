package de.zissithebee.ernst.enums;

public enum Keeper {
    TIME, TITLE, ID,
    SHOW_CALENDAR,
    BUNDLE, IS_UPDATE
}
