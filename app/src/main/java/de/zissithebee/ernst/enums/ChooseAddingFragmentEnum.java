package de.zissithebee.ernst.enums;

public enum ChooseAddingFragmentEnum {
    CHOOSE_ADDING_FRAGMENT, ADD_FOOD, ADD_MAIN_INGREDIENT, ADD_SYMPTOM, ADD_MEDICINE, ADD_NOTE, ADD_DISH
}
