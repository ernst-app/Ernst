package de.zissithebee.ernst.enums;

public enum KeysForEntry {
    ID, DATE, TIME, END_TIME, END_DATE, CATEGORY
}
