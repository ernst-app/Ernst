package de.zissithebee.ernst.helper;

import android.view.View;
import android.widget.ImageView;

public class ClickListenerHelper {

    public void setOnClickListenerImages(int[] ids, View view, View.OnClickListener onClickListener) {
        for (int id : ids) {
            ImageView imageView = (ImageView) view.findViewById(id);
            imageView.setOnClickListener(onClickListener);
        }
    }

}
