package de.zissithebee.ernst.helper;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;

import java.util.Calendar;
import java.util.StringTokenizer;

import de.zissithebee.ernst.AddingActivity;
import de.zissithebee.ernst.receiver.AlarmReceiver;
import de.zissithebee.ernst.MainActivity;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.enums.FoodCategories;
import de.zissithebee.ernst.enums.Keeper;

import static android.content.Context.NOTIFICATION_SERVICE;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_FOOD;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT;

public class NotificationHelper {

    private Context context;
    private String CHANNEL_ID = "ERNST";
    private int ID_BREAKFAST = 1;
    private int ID_LUNCH = 2;
    private int ID_DINNER = 3;
    private int ID_SNACK = 4;
    private SharedPreferencesHelper sharedPreferencesHelper;
    private final int MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24;


    public NotificationHelper(Context context) {
        this.context = context;
        sharedPreferencesHelper = new SharedPreferencesHelper(context);
    }

    public void createNotification(String category) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Intent addNewEntry = new Intent(context, AddingActivity.class);
        Bundle args = new Bundle();
        args.putString(EntryCategories.DISH.toString(), category);
        addNewEntry.putExtra(Keeper.BUNDLE.toString(), args);
        addNewEntry.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), ADD_FOOD.toString());
        PendingIntent addEntryPendingIntent =
                PendingIntent.getActivity(context, 0, addNewEntry, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_ernst_notification)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.notification_hint))
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .addAction(R.drawable.ic_ernst_notification, context.getString(R.string.notification_button), addEntryPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        mBuilder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(1234, mBuilder.build());
    }

    private void setReminderForBreakfast(int hour, int minute) {
        setReminder(hour, minute, ID_BREAKFAST, FoodCategories.BREAKFAST.toString());
    }

    private void setReminderForLunch(int hour, int minute) {
        setReminder(hour, minute, ID_LUNCH, FoodCategories.LUNCH.toString());
    }

    private void setReminderForDinner(int hour, int minute) {
        setReminder(hour, minute, ID_DINNER, FoodCategories.DINNER.toString());
    }

    private void setReminderForSnacks(int hour, int minute) {
        setReminder(hour, minute, ID_SNACK, FoodCategories.SNACK.toString());
    }

    public void setReminder() {

        if (sharedPreferencesHelper.getRememberMe()) {
            if (sharedPreferencesHelper.getRememberMeBreakfast()) {
                String time = sharedPreferencesHelper.getRememberMeBreakfastTime();
                int hour = getHour(time);
                int min = getMinute(time);
                setReminderForBreakfast(hour, min);
            } else {
                tryToCancel(ID_BREAKFAST);
            }
            if (sharedPreferencesHelper.getRememberMeLunch()) {
                String time = sharedPreferencesHelper.getRememberMeLunchTime();
                int hour = getHour(time);
                int min = getMinute(time);
                setReminderForLunch(hour, min);
            } else {
                tryToCancel(ID_LUNCH);
            }
            if (sharedPreferencesHelper.getRememberMeDinner()) {
                String time = sharedPreferencesHelper.getRememberMeDinnerTime();
                int hour = getHour(time);
                int min = getMinute(time);
                setReminderForDinner(hour, min);
            } else {
                tryToCancel(ID_DINNER);
            }
            if (sharedPreferencesHelper.getRememberMeSnacks()) {
                String time = sharedPreferencesHelper.getRememberMeSnacksTime();
                int hour = getHour(time);
                int min = getMinute(time);
                setReminderForSnacks(hour, min);
            } else {
                tryToCancel(ID_SNACK);
            }
        } else {
            tryToCancelAll();
        }
    }

    private void tryToCancelAll() {
        tryToCancel(ID_BREAKFAST);
        tryToCancel(ID_LUNCH);
        tryToCancel(ID_DINNER);
        tryToCancel(ID_SNACK);
    }

    private int getMinute(String time) {
        StringTokenizer stringTokenizer = new StringTokenizer(time, ":");
        stringTokenizer.nextToken();
        return Integer.parseInt(stringTokenizer.nextToken());
    }

    private int getHour(String time) {
        StringTokenizer stringTokenizer = new StringTokenizer(time, ":");
        return Integer.parseInt(stringTokenizer.nextToken());
    }

    private void tryToCancel(int id) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                id, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        pendingIntent.cancel();
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    private void setReminder(int hour, int minute, int id, String category) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        Intent alarmIntent = new Intent(context, AlarmReceiver.class);
        alarmIntent.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), category);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.RTC, calendar.getTimeInMillis(), MILLIS_IN_A_DAY, pendingIntent);
    }


    public void createNotificiationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(description);
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                    NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(mChannel);
        }
    }
}
