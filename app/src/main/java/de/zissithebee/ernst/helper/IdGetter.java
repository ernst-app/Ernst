package de.zissithebee.ernst.helper;

import de.zissithebee.ernst.R;

public class IdGetter {


    public int[] getAddFoodImageIds() {
        int ids[] = {R.id.img_add_food_breakfast, R.id.img_add_food_lunch, R.id.img_add_food_dinner, R.id.img_add_food_snacks};
        return ids;
    }

    public int[] getOverviewFoodImageIds() {
        int ids[] = {R.id.img_breakfast, R.id.img_lunch, R.id.img_dinner, R.id.img_snacks};
        return ids;
    }

    public int[] getSymptomImageIds() {
        int ids[] = {R.id.img_symptom_skin, R.id.img_symptom_respiratory_system, R.id.img_symtpom_gastrointestinal,
                R.id.img_symptom_circulation, R.id.img_symptom_headache, R.id.img_symptom_other};
        return ids;
    }

    public int[] getMedicineImages() {
        int ids[] = {R.id.med_antihistamine, R.id.med_cortisone,
                R.id.med_painkiller, R.id.med_skin_cream,
                R.id.med_gastrointestinal_medicine,
                R.id.med_asthma_medicine, R.id.med_antibiotic,
                R.id.med_cold_medicine, R.id.med_other};
        return ids;
    }
}
