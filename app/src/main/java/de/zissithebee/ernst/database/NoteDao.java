package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNote(Note... notes);

    @Update
    void updateNote(Note... notes);

    @Delete
    void delteNote(Note... notes);

    @Query("SELECT * FROM notes")
    Note[] getAllNotes();

    @Query("SELECT * FROM notes WHERE entry_id = :id")
    Note getNoteById(int id);


}
