package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "notes",
        foreignKeys = @ForeignKey(entity = Entry.class, parentColumns = "id", childColumns = "entry_id", onDelete = CASCADE))
public class Note {

    @PrimaryKey
    @ColumnInfo(name = "entry_id")
    private int entryId;

    @ColumnInfo(name = "note")
    private String note;

    @Ignore
    Entry entry;

    public Note(int entryId, String note) {
        this.entryId = entryId;
        this.note = note;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}
