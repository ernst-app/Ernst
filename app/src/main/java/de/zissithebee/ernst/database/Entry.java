package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.enums.EntryCategories;

@Entity(tableName = "entries")
public class Entry {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "time")
    private String time;

    @ColumnInfo(name = "end_date")
    private String endDate;

    @ColumnInfo(name = "end_time")
    private String endTime;

    @ColumnInfo(name = "category")
    private String category;

    @Ignore
    private DishEaten dishEaten;

    @Ignore
    private Symptom symptom;

    @Ignore
    private Note note;

    @Ignore
    private MedicineTook medicineTook;

    public Entry(String date, String time, String endDate, String endTime, String category) {
        this.date = date;
        this.time = time;
        this.endDate = endDate;
        this.endTime = endTime;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public DishEaten getDishEaten() {
        return dishEaten;
    }

    public void setDishEaten(DishEaten dishEaten) {
        this.dishEaten = dishEaten;
    }

    public Symptom getSymptom() {
        return symptom;
    }

    public void setSymptom(Symptom symptom) {
        this.symptom = symptom;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public MedicineTook getMedicineTook() {
        return medicineTook;
    }

    public void setMedicineTook(MedicineTook medicineTook) {
        this.medicineTook = medicineTook;
    }

    public int getCategoryInSlectedLanguage() {
        if (category.equals(EntryCategories.DISH.toString())) {
            return R.string.fab_eat_drink;
        } else if (category.equals(EntryCategories.SYMPTOM.toString())) {
            return R.string.fab_symptom;
        } else if (category.equals(EntryCategories.MEDICINE.toString())) {
            return R.string.fab_medicine;
        } else {
            return R.string.fab_note;
        }
    }

}
