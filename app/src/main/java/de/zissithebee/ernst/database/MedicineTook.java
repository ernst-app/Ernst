package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import static android.arch.persistence.room.ForeignKey.CASCADE;


@Entity(tableName = "medicinesTook",
        foreignKeys = {@ForeignKey(entity = Medicine.class, parentColumns = "id", childColumns = "medicine_id", onDelete = CASCADE),
                @ForeignKey(entity = Entry.class, parentColumns = "id", childColumns = "entry_id", onDelete = CASCADE)})
public class MedicineTook {

    @PrimaryKey
    @ColumnInfo(name = "entry_id")
    private int entryId;

    @ColumnInfo(name = "medicine_id")
    private int medicineId;

    @ColumnInfo(name = "dose")
    @Nullable
    private String dose;

    @Ignore
    private Medicine medicine;

    @Ignore
    private Entry entry;

    public MedicineTook(int entryId, int medicineId, String dose) {
        this.entryId = entryId;
        this.medicineId = medicineId;
        this.dose = dose;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    @Nullable
    public String getDose() {
        return dose;
    }

    public void setDose(@Nullable String dose) {
        this.dose = dose;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }
}
