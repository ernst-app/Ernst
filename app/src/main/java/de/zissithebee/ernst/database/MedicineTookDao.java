package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface MedicineTookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertMedicineTook(MedicineTook... medicinesTook);

    @Update
    void updateMedicineTook(MedicineTook... medicinesTook);

    @Delete
    void deleteMedicineTook(MedicineTook... medicinesTook);

    @Query("SELECT * FROM medicinesTook")
    MedicineTook[] getAllMedicinesTook();

    @Query("SELECT * FROM medicinesTook WHERE entry_id = :id")
    MedicineTook getMedicineTookById(int id);

    @Query("SELECT medicine_id FROM medicinesTook WHERE entry_id = :id")
    float[] getMedicineTookIds(int id);

}
