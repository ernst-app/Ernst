package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface DishDao {

    @Delete
    void deleteDish(Dish... dishes);

    @Query("SELECT * FROM dishes WHERE id = :id")
    Dish getDishById(int id);

    @Query("SELECT category FROM dishes WHERE id = :id")
    String getCategroyById(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertDishes(Dish dish);

    @Update
    void updateDish(Dish dish);


}
