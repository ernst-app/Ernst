package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface SymptomDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSymptom(Symptom... symptoms);

    @Update
    void updateSymptom(Symptom... symptoms);

    @Delete
    void deleteSymptom(Symptom... symptoms);

    @Query("SELECT * FROM symptoms")
    Symptom[] getAllSymptoms();

    @Query("SELECT * FROM symptoms WHERE entry_id = :id")
    Symptom getSymptomById(int id);

    @Query("SELECT category FROM symptoms WHERE entry_id = :id")
    String getCategoriesById(int id);

}
