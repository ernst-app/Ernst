package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


@Dao
public interface IntoleranceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIntolerance(Intolerance... intolerances);

    @Update
    void updateIntolerance(Intolerance... intolerances);

    @Delete
    void delteIntolerances(Intolerance... intolerances);

    @Query("SELECT * FROM intolerances")
    Intolerance[] getAllIntolerances();

    @Query("SELECT * FROM intolerances WHERE save = 1")
    Intolerance[] getAllIntolerancesSaved();

    @Query("SELECT * FROM intolerances WHERE save = 0")
    Intolerance[] getAllIntoleranceAssumptions();
}
