package de.zissithebee.ernst.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.enums.FoodCategories;


@Entity(tableName = "dishes")
public class Dish {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "category")
    private String category;

    @ColumnInfo(name = "main_ingredients")
    private String mainIngredients;

    @ColumnInfo(name = "ingredients")
    private String ingredients;

    @ColumnInfo(name = "times_eat")
    private int timesEat;

    public Dish(String name, String category, String mainIngredients, String ingredients) {
        this.name = name;
        this.category = category;
        this.mainIngredients = mainIngredients;
        this.ingredients = ingredients;
        timesEat = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMainIngredients() {
        return mainIngredients;
    }

    public void setMainIngredients(String mainIngredients) {
        this.mainIngredients = mainIngredients;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public int getTimesEat() {
        return timesEat;
    }

    public void setTimesEat(int timesEat) {
        this.timesEat = timesEat;
    }

    public int getDishCategoryInSelectedLanguage() {
        if (category.equals(FoodCategories.BREAKFAST.toString())) {
            return R.string.breakfast;
        } else if (category.equals(FoodCategories.LUNCH.toString())) {
            return R.string.lunch;
        } else if (category.equals(FoodCategories.DINNER.toString())) {
            return R.string.dinner;
        } else {
            return R.string.snacks;
        }
    }

    public int getDishCategoryColoredImage() {
        if (category.equals(FoodCategories.BREAKFAST.toString())) {
            return R.drawable.ic_food_breakfast;
        } else if (category.equals(FoodCategories.LUNCH.toString())) {
            return R.drawable.ic_food_lunch;
        } else if (category.equals(FoodCategories.DINNER.toString())) {
            return R.drawable.ic_food_dinner;
        } else {
            return R.drawable.ic_food_snack;
        }
    }

    public int getFrameForImageInAddingActivity() {
        if (category.equals(FoodCategories.BREAKFAST.toString())) {
            return R.id.img_add_food_breakfast;
        } else if (category.equals(FoodCategories.LUNCH.toString())) {
            return R.id.img_add_food_lunch;
        } else if (category.equals(FoodCategories.DINNER.toString())) {
            return R.id.img_add_food_dinner;
        } else {
            return R.id.img_add_food_snacks;
        }
    }
}
