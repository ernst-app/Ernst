package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


@Dao
public interface MedicineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertMedicine(Medicine medicine);

    @Update
    void updateMedicine(Medicine... medicines);

    @Delete
    void deleteMedicine(Medicine... medicines);

    @Query("SELECT * FROM medicines")
    Medicine[] getAllMedicines();

    @Query("SELECT * FROM medicines WHERE id = :id")
    Medicine getMedicineById(int id);

    @Query("SELECT category FROM medicines WHERE id = :id")
    String getCategroyById(int id);


}
