package de.zissithebee.ernst.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


@Dao
public interface DishEatenDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertEatenDish(DishEaten... dishesEaten);

    @Update
    void updateEatenDish(DishEaten... dishesEaten);

    @Delete
    void deleteEatenDish(DishEaten... dishEatens);

    @Query("SELECT * FROM dishesEaten")
    DishEaten[] getAllDishesEaten();

    @Query("SELECT * FROM dishesEaten WHERE entry_id = :id")
    DishEaten getDishEatenById(int id);

    @Query("SELECT dish_id FROM dishesEaten WHERE entry_id = :id")
    float[] getDishIds(int id);

}
