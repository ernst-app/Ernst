package de.zissithebee.ernst.fragments.tourFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.helper.ViewHandler;


public class TourStartNowFragment extends Fragment {

    private View view;
    private ViewHandler viewHandler;
    private TextView tvSuper, tvFirst, tvStartNow, tvHaveFun;
    private List<TextView> tvs;

    private final int DURATION_TO_FADE_FIRST_TV_IN = 1000;
    private final int DURATION_TO_FADE_START_NOW_TV_IN = 3000;
    private final int DURATION_TO_FADE_HAVE_FUN_TV_IN = 3000;


    public TourStartNowFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tour_start_now, container, false);
        viewHandler = new ViewHandler(getActivity());
        initializeViews();
        fadeViewsIn();
        return view;
    }

    private void initializeViews() {
        tvs = new ArrayList<>();
        tvSuper = (TextView) view.findViewById(R.id.welcome_lets_start_super);
        tvs.add(tvSuper);
        tvFirst = (TextView) view.findViewById(R.id.welcome_first);
        tvs.add(tvFirst);
        tvStartNow = (TextView) view.findViewById(R.id.welcome_start_now);
        tvs.add(tvStartNow);
        tvHaveFun = (TextView) view.findViewById(R.id.welcome_have_fun);
        tvs.add(tvHaveFun);
    }

    private void fadeViewsIn() {
        viewHandler.fadeTextIn(tvFirst, DURATION_TO_FADE_FIRST_TV_IN);
        viewHandler.fadeTextIn(tvStartNow, DURATION_TO_FADE_FIRST_TV_IN + DURATION_TO_FADE_START_NOW_TV_IN);
        viewHandler.fadeTextIn(tvHaveFun, DURATION_TO_FADE_FIRST_TV_IN + DURATION_TO_FADE_START_NOW_TV_IN + DURATION_TO_FADE_HAVE_FUN_TV_IN);
    }


}
