package de.zissithebee.ernst.fragments.tourFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.helper.ViewHandler;

public class TourWelcomeFragment extends Fragment {

    private View view;
    private static final int DELAY_FOR_MY_NAME_IS_ERNST = 2000;
    private static final int DELAY_FOR_ESCRIPTION = 2000;

    private ViewHandler viewHandler;

    public TourWelcomeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tour_welcome, container, false);
        viewHandler = new ViewHandler(getActivity());
        startTextViews();
        return view;
    }

    private void startTextViews() {

        TextView tv2 = (TextView) view.findViewById(R.id.tv2_weclome);
        viewHandler.fadeTextIn(tv2, DELAY_FOR_MY_NAME_IS_ERNST);

        TextView tv3 = (TextView) view.findViewById(R.id.tv3_welcome);
        viewHandler.fadeTextIn(tv3, DELAY_FOR_MY_NAME_IS_ERNST + DELAY_FOR_ESCRIPTION);
    }


}
