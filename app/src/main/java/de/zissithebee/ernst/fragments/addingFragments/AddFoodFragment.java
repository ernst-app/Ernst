package de.zissithebee.ernst.fragments.addingFragments;

import android.os.Bundle;

import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import de.zissithebee.ernst.database.Dish;
import de.zissithebee.ernst.database.DishEaten;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.enums.FoodCategories;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForDish;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.helper.IdGetter;
import de.zissithebee.ernst.helper.ViewHandler;
import de.zissithebee.ernst.helper.ClickListenerHelper;
import de.zissithebee.ernst.enums.EntryCategories;


public class AddFoodFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ViewHandler viewHandler;
    private EditText et_food_name, et_food_ingredients;
    private int selectedId;
    private String selectedCategorie;
    private boolean isUpdateView = false;

    public AddFoodFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewHandler = new ViewHandler(getActivity());
        view = inflater.inflate(R.layout.fragment_add_food, container, false);
        int buttonIds[] = {
                R.id.btn_add_food_date, R.id.btn_add_food_time};
        setOnClickListenerButton(buttonIds);
        IdGetter idGetter = new IdGetter();
        int viewIds[] = idGetter.getAddFoodImageIds();
        ClickListenerHelper clickListenerHelper = new ClickListenerHelper();
        clickListenerHelper.setOnClickListenerImages(viewIds, view, this);
        initializeViews();

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            if ((getArguments().getBoolean(Keeper.IS_UPDATE.toString()))) {
                isUpdateView = true;
                fillViesWithArguments();
            }
        } catch (NullPointerException e) {
            //nothing to do here
        }
        try {
            selectViews();
        } catch (NullPointerException e) {
            selectedCategorie = FoodCategories.SNACK.toString();
            selectedId = R.id.img_add_food_snacks;
            setSelected(selectedId);
        }

    }

    private void selectViews() {
        selectedCategorie = getArguments().getString(KeysForDish.CATEGORY.toString());
        if (selectedCategorie.equals(FoodCategories.BREAKFAST.toString())) {
            selectedId = R.id.img_add_food_breakfast;
        } else if (selectedCategorie.equals(FoodCategories.LUNCH.toString())) {
            selectedId = R.id.img_add_food_lunch;
        } else if (selectedCategorie.equals(FoodCategories.DINNER.toString())) {
            selectedId = R.id.img_add_food_dinner;
        } else {
            selectedId = R.id.img_add_food_snacks;
        }
        setSelected(selectedId);
    }

    private void fillViesWithArguments() {
        et_food_name.setText(getArguments().getString(KeysForDish.NAME.toString()));
        et_food_ingredients.setText(getArguments().getString(KeysForDish.INGREDIENTS.toString()));
        Button dateButton = view.findViewById(R.id.btn_add_food_date);
        String date = getArguments().getString(KeysForEntry.DATE.toString());
        dateButton.setText(date);
        String time = getArguments().getString(KeysForEntry.TIME.toString());
        Button timeButton = view.findViewById(R.id.btn_add_food_time);
        timeButton.setText(time);
        setUnselected();
        setSelected(getArguments().getInt(KeysForDish.CATEGORY_IMAGE.toString()));
    }

    private void setOnClickListenerButton(int[] ids) {
        for (int id : ids) {
            Button button = (Button) view.findViewById(id);
            button.setOnClickListener(this);
            if (id == R.id.btn_add_food_time) {
                button.setText(viewHandler.getCurrentTime());
            } else if (id == R.id.btn_add_food_date) {
                button.setText(viewHandler.getCurrentDate());
            }
        }
    }

    private void initializeViews() {
        et_food_name = (EditText) view.findViewById(R.id.eT_add_food_name);
        et_food_ingredients = (EditText) view.findViewById(R.id.et_add_food_ingredients);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        switch (id) {
            case R.id.btn_add_food_date:
                viewHandler.showDatePickerFragment((Button) view.findViewById(id), this);
                break;
            case R.id.btn_add_food_time:
                viewHandler.showTimePickerFrgament((Button) view.findViewById(id), this);
                break;
            case R.id.img_add_food_breakfast:
            case R.id.img_add_food_lunch:
            case R.id.img_add_food_dinner:
            case R.id.img_add_food_snacks:
                setUnselected();
                setSelected(id);
                break;
        }
    }


    private void setUnselected() {
        ImageView selectedView = view.findViewById(selectedId);
        switch (selectedId) {
            case R.id.img_add_food_breakfast:
                selectedView.setImageResource(R.drawable.ic_food_breakfast_bw);
                break;
            case R.id.img_add_food_lunch:
                selectedView.setImageResource(R.drawable.ic_food_lunch_bw);
                break;
            case R.id.img_add_food_dinner:
                selectedView.setImageResource(R.drawable.ic_food_dinner_bw);
                break;
            case R.id.img_add_food_snacks:
                selectedView.setImageResource(R.drawable.ic_food_snack_bw);
                break;
        }

    }

    private void setSelected(int id) {
        selectedId = id;
        ImageView imageView = (ImageView) view.findViewById(id);
        switch (id) {
            case R.id.img_add_food_breakfast:
                selectedCategorie = FoodCategories.BREAKFAST.toString();
                imageView.setImageResource(R.drawable.ic_food_breakfast);
                break;
            case R.id.img_add_food_lunch:
                selectedCategorie = FoodCategories.LUNCH.toString();
                imageView.setImageResource(R.drawable.ic_food_lunch);
                break;
            case R.id.img_add_food_dinner:
                selectedCategorie = FoodCategories.DINNER.toString();
                imageView.setImageResource(R.drawable.ic_food_dinner);
                break;
            case R.id.img_add_food_snacks:
                selectedCategorie = FoodCategories.SNACK.toString();
                imageView.setImageResource(R.drawable.ic_food_snack);
        }
    }

    public void checkSave() {
        String title = et_food_name.getText().toString();
        if (title.length() == 0) {
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.hint_enter_name_food), Toast.LENGTH_LONG).show();
        } else if (selectedCategorie == null) {
            Toast.makeText(getActivity(), getString(R.string.hint_enter_categoy_food), Toast.LENGTH_LONG).show();
        } else {
            save();
            getActivity().finish();
        }
    }

    private void save() {
        final Button date = (Button) view.findViewById(R.id.btn_add_food_date);
        final Button time = (Button) view.findViewById(R.id.btn_add_food_time);
        Dish dish = new Dish(et_food_name.getText().toString(), selectedCategorie, null, et_food_ingredients.getText().toString());
        final String dateForSave = date.getText().toString();
        final String timeForSave = time.getText().toString();

        if (isUpdateView) {
            dish.setId(getArguments().getInt(KeysForDish.ID_DISH.toString()));
            Entry entry = new Entry(dateForSave, timeForSave, null, null, EntryCategories.DISH.toString());
            entry.setId(getArguments().getInt(KeysForEntry.ID.toString()));
            DishEaten dishEaten = new DishEaten(entry.getId(), dish.getId());
            updateEntry(dish, entry, dishEaten);
        } else {
            createNewEntry(dish, dateForSave, timeForSave);
        }
    }

    private void updateEntry(final Dish dish, final Entry entry, final DishEaten dishEaten) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                ErnstDatabase.getInstance(getActivity())
                        .dishDao()
                        .updateDish(dish);
                ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .updateEntry(entry);
                ErnstDatabase.getInstance(getActivity())
                        .dishEatenDao()
                        .updateEatenDish(dishEaten);
            }
        }).start();
    }

    private void createNewEntry(final Dish dish, final String dateForSave, final String timeForSave) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                long dishId = ErnstDatabase.getInstance(getActivity())
                        .dishDao()
                        .insertDishes(dish);
                Entry entry = new Entry(dateForSave, timeForSave, null, null, EntryCategories.DISH.toString());
                long entryId = ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .insertEntry(entry);
                DishEaten eatenDish = new DishEaten((int) entryId, (int) dishId);
                ErnstDatabase.getInstance(getActivity())
                        .dishEatenDao()
                        .insertEatenDish(eatenDish);
            }
        }).start();
    }


}
