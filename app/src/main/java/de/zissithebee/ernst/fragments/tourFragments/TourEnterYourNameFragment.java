package de.zissithebee.ernst.fragments.tourFragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.helper.SharedPreferencesHelper;
import de.zissithebee.ernst.helper.ViewHandler;

public class TourEnterYourNameFragment extends Fragment {

    private EditText et;

    private final int DELAY_TO_FADE_EDIT_TEXT_IN = 1000;

    private SharedPreferencesHelper sharedPreferencesHelper;

    public TourEnterYourNameFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewHandler viewHandler = new ViewHandler(getActivity());
        View view = inflater.inflate(R.layout.fragment_tour_enter_your_name, container, false);
        sharedPreferencesHelper = new SharedPreferencesHelper(getActivity());
        et = (EditText) view.findViewById(R.id.eT_enter_name);
        et.setText(sharedPreferencesHelper.getName());
        viewHandler.fadeTextIn(et, DELAY_TO_FADE_EDIT_TEXT_IN);
        return view;
    }

    public void saveName() {
        String name = et.getText().toString();
        sharedPreferencesHelper.setName(name);
    }

}
