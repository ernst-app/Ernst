package de.zissithebee.ernst.fragments.tourFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.fragments.pickerFragments.TimePickerFragment;
import de.zissithebee.ernst.helper.NotificationHelper;
import de.zissithebee.ernst.helper.SharedPreferencesHelper;
import de.zissithebee.ernst.helper.ViewHandler;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class TourRememberMeSettingsFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private View view;
    private ViewHandler viewHandler;

    private SharedPreferencesHelper sharedPreferencesHelper;

    private Switch switchRememberMe, switchRememberBreakfast, switchRememberLunch, switchRememberDinner, switchRememberSnacks;
    private TextView tvIShouldRemember, tvRememberMeals, tvBreakfast, tvLunch, tvDinner, tvSnacks;
    private Button btnBreakfastTime, btnLunchTime, btnDinnerTime, btnSnackTime;

    private List<TextView> tvs;
    private List<Button> bttns;


    public TourRememberMeSettingsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferencesHelper = new SharedPreferencesHelper(getActivity());
        view = inflater.inflate(R.layout.fragment_remember_me_settings, container, false);
        viewHandler = new ViewHandler(getActivity());
        initializeViews();
        return view;
    }


    private void initializeViews() {
        initializeSwitches();
        initializeTextViews();
        initializeButtons();
    }

    private void initializeSwitches() {
        switchRememberMe = (Switch) view.findViewById(R.id.switch_remember);
        switchRememberMe.setOnCheckedChangeListener(this);
        switchRememberMe.setChecked(sharedPreferencesHelper.getRememberMe());

        switchRememberBreakfast = (Switch) view.findViewById(R.id.switch_breakfast);
        switchRememberBreakfast.setOnCheckedChangeListener(this);
        switchRememberBreakfast.setChecked(sharedPreferencesHelper.getRememberMeBreakfast());

        switchRememberLunch = (Switch) view.findViewById(R.id.switch_lunch);
        switchRememberLunch.setOnCheckedChangeListener(this);
        switchRememberLunch.setChecked(sharedPreferencesHelper.getRememberMeLunch());

        switchRememberDinner = (Switch) view.findViewById(R.id.switch_dinner);
        switchRememberDinner.setOnCheckedChangeListener(this);
        switchRememberDinner.setChecked(sharedPreferencesHelper.getRememberMeDinner());

        switchRememberSnacks = (Switch) view.findViewById(R.id.switch_snacks);
        switchRememberSnacks.setOnCheckedChangeListener(this);
        switchRememberSnacks.setChecked(sharedPreferencesHelper.getRememberMeSnacks());
    }

    private void initializeTextViews() {
        tvs = new ArrayList<>();
        tvIShouldRemember = (TextView) view.findViewById(R.id.welcome_should_i_remember);
        tvs.add(tvIShouldRemember);
        tvRememberMeals = (TextView) view.findViewById(R.id.welcome_remember_meals);
        tvs.add(tvRememberMeals);
        tvBreakfast = (TextView) view.findViewById(R.id.welcome_breakfast);
        tvs.add(tvBreakfast);
        tvLunch = (TextView) view.findViewById(R.id.welcome_lunch);
        tvs.add(tvLunch);
        tvDinner = (TextView) view.findViewById(R.id.welcome_dinner);
        tvs.add(tvDinner);
        tvSnacks = (TextView) view.findViewById(R.id.welcome_snack);
        tvs.add(tvSnacks);
    }

    private void initializeButtons() {
        bttns = new ArrayList<>();

        btnBreakfastTime = (Button) view.findViewById(R.id.btn_breakfast_time);
        btnBreakfastTime.setText(sharedPreferencesHelper.getRememberMeBreakfastTime());
        btnBreakfastTime.setOnClickListener(this);
        bttns.add(btnBreakfastTime);

        btnLunchTime = (Button) view.findViewById(R.id.btn_lunch_time);
        btnLunchTime.setText(sharedPreferencesHelper.getRememberMeLunchTime());
        btnLunchTime.setOnClickListener(this);
        bttns.add(btnLunchTime);

        btnDinnerTime = (Button) view.findViewById(R.id.btn_dinner_time);
        btnDinnerTime.setText(sharedPreferencesHelper.getRememberMeDinnerTime());
        btnDinnerTime.setOnClickListener(this);
        bttns.add(btnDinnerTime);

        btnSnackTime = (Button) view.findViewById(R.id.btn_snacks_time);
        btnSnackTime.setText(sharedPreferencesHelper.getRememberMeSnacksTime());
        btnSnackTime.setOnClickListener(this);
        bttns.add(btnSnackTime);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        int id = compoundButton.getId();
        switch (id) {
            case R.id.switch_remember:
                decideToShowViews(b);
                break;
            case R.id.switch_breakfast:
                decideToEnableButton(b, R.id.btn_breakfast_time);
                break;
            case R.id.switch_dinner:
                decideToEnableButton(b, R.id.btn_dinner_time);
                break;
            case R.id.switch_lunch:
                decideToEnableButton(b, R.id.btn_lunch_time);
                break;
            case R.id.switch_snacks:
                decideToEnableButton(b, R.id.btn_snacks_time);
                break;
        }
    }

    private void decideToEnableButton(boolean enableButton, int btnId) {
        Button button = view.findViewById(btnId);
        if (enableButton) {
            button.setEnabled(true);
        } else {
            button.setEnabled(false);
        }
    }

    private void decideToShowViews(Boolean showViews) {
        if (!showViews) {
            tvRememberMeals.setVisibility(GONE);

            switchRememberBreakfast.setVisibility(GONE);
            switchRememberLunch.setVisibility(GONE);
            switchRememberDinner.setVisibility(GONE);
            switchRememberSnacks.setVisibility(GONE);

            tvBreakfast.setVisibility(GONE);
            tvLunch.setVisibility(GONE);
            tvDinner.setVisibility(GONE);
            tvSnacks.setVisibility(GONE);

            btnBreakfastTime.setVisibility(GONE);
            btnLunchTime.setVisibility(GONE);
            btnDinnerTime.setVisibility(GONE);
            btnSnackTime.setVisibility(GONE);

        } else {
            tvRememberMeals.setVisibility(VISIBLE);

            switchRememberBreakfast.setVisibility(VISIBLE);
            switchRememberLunch.setVisibility(VISIBLE);
            switchRememberDinner.setVisibility(VISIBLE);
            switchRememberSnacks.setVisibility(VISIBLE);

            tvBreakfast.setVisibility(VISIBLE);
            tvLunch.setVisibility(VISIBLE);
            tvDinner.setVisibility(VISIBLE);
            tvSnacks.setVisibility(VISIBLE);

            btnBreakfastTime.setVisibility(VISIBLE);
            btnLunchTime.setVisibility(VISIBLE);
            btnDinnerTime.setVisibility(VISIBLE);
            btnSnackTime.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(View view) {
        TimePickerFragment timePickerFragment = new TimePickerFragment((Button) view);
        timePickerFragment.show(getActivity().getSupportFragmentManager(), "timePicker");
    }

    public void saveSettings() {
        sharedPreferencesHelper.setRememberMe(switchRememberMe.isChecked());

        sharedPreferencesHelper.setRememberMeBreakfast(switchRememberBreakfast.isChecked());
        sharedPreferencesHelper.setRememberMeBreakfastTime(btnBreakfastTime.getText().toString());

        sharedPreferencesHelper.setRememberMeLunch(switchRememberLunch.isChecked());
        sharedPreferencesHelper.setRememberMeLunchTime(btnLunchTime.getText().toString());

        sharedPreferencesHelper.setRememberMeDinner(switchRememberDinner.isChecked());
        sharedPreferencesHelper.setRememberMeDinnerTime(btnDinnerTime.getText().toString());

        sharedPreferencesHelper.setRememberMeSnacks(switchRememberSnacks.isChecked());
        sharedPreferencesHelper.setRememberMeSnacksTime(btnSnackTime.getText().toString());
    }

    public void startReminder() {
        NotificationHelper notificationHelper = new NotificationHelper(getContext());
        notificationHelper.createNotificiationChannel();
        notificationHelper.setReminder();
    }
}
