package de.zissithebee.ernst.fragments;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import com.github.clans.fab.FloatingActionButton;

import de.zissithebee.ernst.AddingActivity;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.fragments.drawerFragments.DetailsFragment;
import de.zissithebee.ernst.fragments.drawerFragments.OverviewFragment;

import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_FOOD;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_MEDICINE;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_NOTE;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.ADD_SYMPTOM;
import static de.zissithebee.ernst.enums.ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT;
import static de.zissithebee.ernst.enums.Keeper.SHOW_CALENDAR;

public class MainFragment extends Fragment implements View.OnClickListener {

    private static final int NUM_PAGES = 2;
    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private ScreenSlidePagerAdapter adapter;

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        pagerAdapter = new ScreenSlidePagerAdapter(getActivity().getSupportFragmentManager());
        setupViewPager();
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.colorAccent));
        tabLayout.setupWithViewPager(viewPager);
        FloatingActionButton fabEatDrink = (FloatingActionButton) view.findViewById(R.id.fab_eat_drink);
        fabEatDrink.setOnClickListener(this);
        FloatingActionButton fabSymptom = (FloatingActionButton) view.findViewById(R.id.fab_symptom);
        fabSymptom.setOnClickListener(this);
        FloatingActionButton fabMedicine = (FloatingActionButton) view.findViewById(R.id.fab_medicine);
        fabMedicine.setOnClickListener(this);
        FloatingActionButton fabNote = (FloatingActionButton) view.findViewById(R.id.fab_note);
        fabNote.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Intent intent = new Intent(getActivity(), AddingActivity.class);

        switch (id) {
            case R.id.fab_eat_drink:
                intent.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), ADD_FOOD.toString());
                break;
            case R.id.fab_symptom:
                intent.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), ADD_SYMPTOM.toString());
                break;
            case R.id.fab_medicine:
                intent.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), ADD_MEDICINE.toString());
                break;
            case R.id.fab_note:
                intent.putExtra(CHOOSE_ADDING_FRAGMENT.toString(), ADD_NOTE.toString());
                break;
        }

        startActivity(intent);
    }

    private void setupViewPager() {
        adapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
        DetailsFragment detailsFragment = new DetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(SHOW_CALENDAR.toString(), false);
        detailsFragment.setArguments(bundle);
        adapter.addFragment(new OverviewFragment(), getString(R.string.tab_overview));
        adapter.addFragment(detailsFragment, getString(R.string.tab_details));
        viewPager.setAdapter(adapter);
    }

    public List<Fragment> getFragments() {
        return adapter.getAllFragments();
    }

    public class ScreenSlidePagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> fragmentTitleList = new ArrayList<>();

        private ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public List<Fragment> getAllFragments() {
            return fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }

        public void addFragment(Fragment fragment, String title) {
            fragmentList.add(fragment);
            fragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitleList.get(position);
        }
    }

}
