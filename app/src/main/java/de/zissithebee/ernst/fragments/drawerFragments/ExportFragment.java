package de.zissithebee.ernst.fragments.drawerFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.asynctasks.GetEntriesForExport;


public class ExportFragment extends Fragment implements View.OnClickListener {

    private boolean exportFood = true;
    private boolean exportSymptom = true;
    private boolean exportMedicine = true;
    private boolean exportNotes = true;

    private View fragmentView;
    private ProgressBar pB;

    public ExportFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_export, container, false);
        Button buttonExport = (Button) fragmentView.findViewById(R.id.btn_export);
        buttonExport.setOnClickListener(this);
        pB = fragmentView.findViewById(R.id.export_progress);
        TextView tvExportEatenDishes = fragmentView.findViewById(R.id.export_eaten_dishes_text);
        tvExportEatenDishes.setOnClickListener(this);
        TextView tvExportSympotms = fragmentView.findViewById(R.id.export_symptoms_text);
        tvExportSympotms.setOnClickListener(this);
        TextView tvExportMedicines = fragmentView.findViewById(R.id.export_medicine_text);
        tvExportMedicines.setOnClickListener(this);
        TextView tvExportNotes = fragmentView.findViewById(R.id.export_notes_text);
        tvExportNotes.setOnClickListener(this);
        return fragmentView;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        CheckBox cb;
        switch (id) {
            case R.id.btn_export:
                getExportCategories();
                pB.setVisibility(View.VISIBLE);
                GetEntriesForExport getEntriesForExport = new GetEntriesForExport(exportFood, exportMedicine, exportSymptom, exportNotes);
                getEntriesForExport.execute(this);
                break;
            case R.id.export_eaten_dishes_text:
                cb = fragmentView.findViewById(R.id.export_eaten_dishes);
                setCheckedOrUnchecked(cb);
                break;
            case R.id.export_symptoms_text:
                cb = fragmentView.findViewById(R.id.export_symptoms);
                setCheckedOrUnchecked(cb);
                break;
            case R.id.export_medicine_text:
                cb = fragmentView.findViewById(R.id.export_medicine);
                setCheckedOrUnchecked(cb);
                break;
            case R.id.export_notes_text:
                cb = fragmentView.findViewById(R.id.export_notes);
                setCheckedOrUnchecked(cb);
                break;
        }
    }

    private void setCheckedOrUnchecked(CheckBox cb) {
        if (cb.isChecked()) {
            cb.setChecked(false);
        } else {
            cb.setChecked(true);
        }
    }

    private void getExportCategories() {
        CheckBox cbFood = (CheckBox) fragmentView.findViewById(R.id.export_eaten_dishes);
        exportFood = cbFood.isChecked();
        CheckBox cbSymptoms = (CheckBox) fragmentView.findViewById(R.id.export_symptoms);
        exportSymptom = cbSymptoms.isChecked();
        CheckBox cbMedicine = (CheckBox) fragmentView.findViewById(R.id.export_medicine);
        exportMedicine = cbMedicine.isChecked();
        CheckBox cbNotes = (CheckBox) fragmentView.findViewById(R.id.export_notes);
        exportNotes = cbNotes.isChecked();
    }

    public void sendEmail() {
        pB.setVisibility(View.GONE);
        File exportFile = new File(getContext().getFilesDir() + "/ernst.csv");
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        try {
            Uri fileUri = FileProvider.getUriForFile(
                    getContext(),
                    "de.zissithebee.ernst.fileprovider",
                    exportFile);
            i.putExtra(Intent.EXTRA_STREAM, fileUri);
        } catch (IllegalArgumentException e) {
            Toast.makeText(getContext(), R.string.error_loading_file, Toast.LENGTH_SHORT).show();
        }
        try {
            startActivity(Intent.createChooser(i, getString(R.string.export_choose_sharing)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), R.string.error_sharing, Toast.LENGTH_SHORT).show();
        }
    }
}
