package de.zissithebee.ernst.fragments.drawerFragments;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.asynctasks.GetEntriesOnSpecialDay;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.fragments.cardFragments.DishEatenCardFragment;
import de.zissithebee.ernst.fragments.cardFragments.MedicineCardFragment;
import de.zissithebee.ernst.fragments.cardFragments.NoteCardFragment;
import de.zissithebee.ernst.fragments.cardFragments.SymptomCardFragment;
import de.zissithebee.ernst.helper.ViewHandler;

import static de.zissithebee.ernst.enums.Keeper.SHOW_CALENDAR;

public class DetailsFragment extends Fragment {

    private ViewHandler viewHandler;
    private View view;
    private boolean calendarVisible;
    private ProgressBar detailsProgress;


    public DetailsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_details, container, false);
        detailsProgress = view.findViewById(R.id.progress_details);
        viewHandler = new ViewHandler(getContext());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            if (getArguments().getBoolean(SHOW_CALENDAR.toString())) {
                showCalendar();
                calendarVisible = true;
            } else {
                hideCalendar();
                calendarVisible = false;
            }
        }
        super.onViewCreated(view, savedInstanceState);
    }

    private void showCalendar() {
        CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendar_view);
        if (Build.VERSION.SDK_INT <= 22) {
            ViewGroup.LayoutParams params = calendarView.getLayoutParams();
            params.height = 800;
            calendarView.setLayoutParams(params);
        }
        calendarView.setVisibility(View.VISIBLE);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                loadEntries(day, month, year);
            }
        });
    }

    private void hideCalendar() {
        CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendar_view);
        calendarView.setVisibility(View.GONE);
    }


    private void loadEntries(int day, int month, int year) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.GERMAN);
        Date date = new Date(year - 1900, month, day);
        new GetEntriesOnSpecialDay(sdf.format(date)).execute(this);
        detailsProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        execute();
    }

    public void execute() {
        detailsProgress.setVisibility(View.VISIBLE);
        new GetEntriesOnSpecialDay(viewHandler.getCurrentDate()).execute(this);
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    public void updateView(Entry[] entries) {
        try {
            LinearLayout linlay = (LinearLayout) view.findViewById(R.id.lin_lay);
            if (linlay != null && linlay.getChildCount() > 0) {
                linlay.removeAllViews();
            }
        } catch (Exception e) {
            //nothing to do here
        }
        CardView no_entries_hin = view.findViewById(R.id.card_no_entries);
        if (entries.length != 0) {
            no_entries_hin.setVisibility(View.GONE);
        } else {
            no_entries_hin.setVisibility(View.VISIBLE);
        }
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        for (Entry entry : entries) {
            if(entry != null){
                Bundle args = new Bundle();
                args.putBoolean("Calendar", calendarVisible);
                if (entry.getCategory().equals(EntryCategories.DISH.toString())) {
                    DishEatenCardFragment dishEatenCardFragment = new DishEatenCardFragment(entry);
                    dishEatenCardFragment.setArguments(args);
                    fragmentTransaction.add(R.id.lin_lay, dishEatenCardFragment, Integer.toString(entry.getId()));
                } else if (entry.getCategory().equals(EntryCategories.SYMPTOM.toString())) {
                    SymptomCardFragment symptomCardFragment = new SymptomCardFragment(entry);
                    symptomCardFragment.setArguments(args);
                    fragmentTransaction.add(R.id.lin_lay, symptomCardFragment, Integer.toString(entry.getId()));
                } else if (entry.getCategory().equals(EntryCategories.MEDICINE.toString())) {
                    MedicineCardFragment medicineCardFragment = new MedicineCardFragment(entry);
                    medicineCardFragment.setArguments(args);
                    fragmentTransaction.add(R.id.lin_lay, medicineCardFragment, Integer.toString(entry.getId()));
                } else if (entry.getCategory().equals(EntryCategories.NOTE.toString())) {
                    NoteCardFragment noteCardFragment = new NoteCardFragment(entry);
                    noteCardFragment.setArguments(args);
                    fragmentTransaction.add(R.id.lin_lay, noteCardFragment, Integer.toString(entry.getId()));
                }
            }
        }
        detailsProgress.setVisibility(View.GONE);
        fragmentTransaction.commit();
    }

}
