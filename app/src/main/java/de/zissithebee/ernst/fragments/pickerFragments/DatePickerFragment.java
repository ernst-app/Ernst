package de.zissithebee.ernst.fragments.pickerFragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Button;
import android.widget.DatePicker;

import java.util.Calendar;


public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private Button button;

    public DatePickerFragment() {

    }

    public DatePickerFragment(Button button) {
        this.button = button;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        String monthString;
        String dayString;
        String yearString;
        month = month + 1;
        if (button != null) {
            if (month < 10) {
                monthString = "0" + Integer.toString(month);
            } else {
                monthString = Integer.toString(month);
            }
            if (day < 10) {
                dayString = "0" + Integer.toString(day);
            } else {
                dayString = Integer.toString(day);
            }
            if (year < 10) {
                yearString = "000" + Integer.toString(year);
            } else if (year < 100) {
                yearString = "00" + Integer.toString(year);
            } else if (year < 1000) {
                yearString = "0" + Integer.toString(year);
            } else {
                yearString = Integer.toString(year);
            }
            button.setText(dayString + "." + monthString + "." + yearString);
        }
    }
}
