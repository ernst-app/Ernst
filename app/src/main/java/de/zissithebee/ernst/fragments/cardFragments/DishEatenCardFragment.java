package de.zissithebee.ernst.fragments.cardFragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.AddingActivity;
import de.zissithebee.ernst.enums.ChooseAddingFragmentEnum;
import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.dialogs.DeleteDialog;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForDish;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.fragments.MainFragment;
import de.zissithebee.ernst.fragments.drawerFragments.DetailsFragment;

import static de.zissithebee.ernst.enums.Keeper.ID;
import static de.zissithebee.ernst.enums.Keeper.TIME;
import static de.zissithebee.ernst.enums.Keeper.TITLE;


public class DishEatenCardFragment extends Fragment implements View.OnClickListener {


    private View view;
    private Entry entry;


    public DishEatenCardFragment() {
    }

    public DishEatenCardFragment(Entry entry) {
        this.entry = entry;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.card_eaten_dishes, container, false);
        fillTVs(entry);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageButton removeBtn = (ImageButton) view.findViewById(R.id.btn_remove_eaten_dish);
        removeBtn.setOnClickListener(this);
        ImageButton editButton = (ImageButton) view.findViewById(R.id.btn_edit_eaten_dish);
        editButton.setOnClickListener(this);
    }

    private void fillTVs(Entry entry) {
        TextView categoryTv = (TextView) view.findViewById(R.id.tv_ec_category);
        categoryTv.setText(entry.getDishEaten().getDish().getDishCategoryInSelectedLanguage());
        ImageView imageView = (ImageView) view.findViewById(R.id.img_ec);
        imageView.setImageResource(entry.getDishEaten().getDish().getDishCategoryColoredImage());
        TextView title = (TextView) view.findViewById(R.id.tv_ec_title);
        title.setText(entry.getDishEaten().getDish().getName());
        TextView ingredients = (TextView) view.findViewById(R.id.tv_ec_ingredients);
        ingredients.setText(entry.getDishEaten().getDish().getIngredients());
        TextView time = (TextView) view.findViewById(R.id.tv_ec_time);
        time.setText(entry.getTime());
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_remove_eaten_dish:
                deleteEntry();
                break;
            case R.id.btn_edit_eaten_dish:
                editEntry();
                break;

        }
    }

    private void deleteEntry() {
        List<Fragment> fragments = getFragments();
        DeleteDialog deleteDialog = new DeleteDialog(fragments);
        Bundle args = new Bundle();
        args.putInt(ID.toString(), entry.getId());
        args.putString(TIME.toString(), entry.getDate() + " " + entry.getTime());
        args.putString(TITLE.toString(), getString(entry.getCategoryInSlectedLanguage()));
        deleteDialog.setArguments(args);
        deleteDialog.show(getActivity().getSupportFragmentManager(), "dialogDelete");
    }

    private void editEntry() {
        Bundle entryToEdit = entryBundle();
        Intent intent = new Intent(getActivity(), AddingActivity.class);
        intent.putExtra(Keeper.BUNDLE.toString(), entryToEdit);
        intent.putExtra(ChooseAddingFragmentEnum.CHOOSE_ADDING_FRAGMENT.toString(), ChooseAddingFragmentEnum.ADD_FOOD.toString());
        startActivity(intent);
    }

    private List<Fragment> getFragments() {
        if (getArguments().getBoolean("Calendar")) {
            DetailsFragment detailsFragment = (DetailsFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            List<Fragment> thisFragment = new ArrayList();
            thisFragment.add(detailsFragment);
            return thisFragment;
        } else {
            MainFragment mainFragment = (MainFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.frame_layout);
            return mainFragment.getFragments();
        }
    }

    private Bundle entryBundle() {
        Bundle entryBundle = new Bundle();
        entryBundle.putInt(KeysForEntry.ID.toString(), entry.getId());
        entryBundle.putString(KeysForEntry.DATE.toString(), entry.getDate());
        entryBundle.putString(KeysForEntry.TIME.toString(), entry.getTime());

        entryBundle.putString(KeysForDish.CATEGORY.toString(), entry.getDishEaten().getDish().getCategory());
        entryBundle.putString(KeysForDish.INGREDIENTS.toString(), entry.getDishEaten().getDish().getIngredients());
        entryBundle.putString(KeysForDish.MAIN_INGREDIENTS.toString(), entry.getDishEaten().getDish().getMainIngredients());
        entryBundle.putString(KeysForDish.NAME.toString(), entry.getDishEaten().getDish().getName());
        entryBundle.putInt(KeysForDish.ID_DISH.toString(), entry.getDishEaten().getDishId());
        entryBundle.putInt(KeysForDish.CATEGORY_IMAGE.toString(), entry.getDishEaten().getDish().getFrameForImageInAddingActivity());

        entryBundle.putBoolean(Keeper.IS_UPDATE.toString(), true);

        return entryBundle;
    }

}
