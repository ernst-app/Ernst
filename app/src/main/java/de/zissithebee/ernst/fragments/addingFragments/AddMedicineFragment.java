package de.zissithebee.ernst.fragments.addingFragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.database.Medicine;
import de.zissithebee.ernst.database.MedicineTook;
import de.zissithebee.ernst.enums.EntryCategories;
import de.zissithebee.ernst.enums.Keeper;
import de.zissithebee.ernst.enums.KeysForEntry;
import de.zissithebee.ernst.enums.KeysForMedicine;
import de.zissithebee.ernst.enums.MedicineCategories;
import de.zissithebee.ernst.helper.ClickListenerHelper;
import de.zissithebee.ernst.helper.IdGetter;
import de.zissithebee.ernst.helper.ViewHandler;

public class AddMedicineFragment extends Fragment implements View.OnClickListener {


    private Button btn_add_med_date, btn_add_med_time;
    private ViewHandler viewHandler;
    private View view;
    private int lastSelectedMedicineId;
    private String lastSelectedMedicine;
    private boolean isUpdateView = false;


    public AddMedicineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_medicine, container, false);
        viewHandler = new ViewHandler(getActivity());
        inititalizeViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            if (getArguments().getBoolean(Keeper.IS_UPDATE.toString())) {
                isUpdateView = true;
                fillViesWithArguments();
            }
        } catch (NullPointerException e) {
            //nothing to do here
        }
    }

    private void fillViesWithArguments() {
        EditText et_medicine_name = (EditText) view.findViewById(R.id.medicine_name);
        et_medicine_name.setText(getArguments().getString(KeysForMedicine.NAME.toString()));

        EditText et_medicine_dose = (EditText) view.findViewById(R.id.medicine_dose);
        et_medicine_dose.setText(getArguments().getString(KeysForMedicine.DOSE.toString()));

        setUnselected();
        setSelected(getArguments().getInt(KeysForMedicine.CATEGORY_IMAGE.toString()));

        btn_add_med_date.setText(getArguments().getString(KeysForEntry.DATE.toString()));
        btn_add_med_time.setText(getArguments().getString(KeysForEntry.TIME.toString()));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void inititalizeViews() {
        IdGetter idGetter = new IdGetter();
        ClickListenerHelper clickListenerHelper = new ClickListenerHelper();
        int ids[] = idGetter.getMedicineImages();
        lastSelectedMedicineId = R.id.med_other;
        lastSelectedMedicine = MedicineCategories.OTHER.toString();
        clickListenerHelper.setOnClickListenerImages(ids, view, this);

        btn_add_med_date = (Button) view.findViewById(R.id.btn_add_med_date);
        btn_add_med_date.setOnClickListener(this);
        btn_add_med_date.setText(viewHandler.getCurrentDate());

        btn_add_med_time = (Button) view.findViewById(R.id.btn_add_med_time);
        btn_add_med_time.setOnClickListener(this);
        btn_add_med_time.setText(viewHandler.getCurrentTime());


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_add_med_date:
                viewHandler.showDatePickerFragment((Button) view.findViewById(id), this);
                break;
            case R.id.btn_add_med_time:
                viewHandler.showTimePickerFrgament((Button) view.findViewById(id), this);
                break;
            case R.id.med_antihistamine:
            case R.id.med_cortisone:
            case R.id.med_painkiller:
            case R.id.med_skin_cream:
            case R.id.med_gastrointestinal_medicine:
            case R.id.med_asthma_medicine:
            case R.id.med_antibiotic:
            case R.id.med_cold_medicine:
            case R.id.med_other:
                setUnselected();
                setSelected(id);
        }

    }

    private void setUnselected() {
        ImageView lastSelectedView = view.findViewById(lastSelectedMedicineId);
        switch (lastSelectedMedicineId) {
            case R.id.med_antihistamine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_antihistamine_bw);
                break;
            case R.id.med_cortisone:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_cortisone_bw);
                break;
            case R.id.med_painkiller:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_painkiller_bw);
                break;
            case R.id.med_skin_cream:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_skin_cream_bw);
                break;
            case R.id.med_gastrointestinal_medicine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_gastrointestinal_medicine_bw);
                break;
            case R.id.med_asthma_medicine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_asthma_medicine_bw);
                break;
            case R.id.med_antibiotic:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_antibiotic_bw);
                break;
            case R.id.med_cold_medicine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_cold_bw);
                break;
            case R.id.med_other:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_other_bw);
                break;
        }
    }

    private void setSelected(int id) {
        lastSelectedMedicineId = id;
        ImageView lastSelectedView = view.findViewById(id);

        switch (id) {
            case R.id.med_antihistamine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_antihistamine);
                lastSelectedMedicine = MedicineCategories.ANTIHISTAMINE.toString();
                break;
            case R.id.med_cortisone:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_cortisone);
                lastSelectedMedicine = MedicineCategories.CORTISONE.toString();
                break;
            case R.id.med_painkiller:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_painkiller);
                lastSelectedMedicine = MedicineCategories.PAINKILLER.toString();
                break;
            case R.id.med_skin_cream:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_skin_cream);
                lastSelectedMedicine = MedicineCategories.SKIN_CREAM.toString();
                break;
            case R.id.med_gastrointestinal_medicine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_gastrointestinal_medicine);
                lastSelectedMedicine = MedicineCategories.GASTROINTESTINAL.toString();
                break;
            case R.id.med_asthma_medicine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_asthma_medicine);
                lastSelectedMedicine = MedicineCategories.ASTHMA.toString();
                break;
            case R.id.med_antibiotic:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_antibiotic);
                lastSelectedMedicine = MedicineCategories.ANTIBIOTIC.toString();
                break;
            case R.id.med_cold_medicine:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_cold);
                lastSelectedMedicine = MedicineCategories.COLD_MED.toString();
                break;
            case R.id.med_other:
                lastSelectedView.setImageResource(R.drawable.ic_medicine_other);
                lastSelectedMedicine = MedicineCategories.OTHER.toString();
                break;
        }
    }

    public void checkSave() {
        String dose = ((EditText) view.findViewById(R.id.medicine_dose)).getText().toString();
        String name = ((EditText) view.findViewById(R.id.medicine_name)).getText().toString();
        String category = lastSelectedMedicine;
        String date = btn_add_med_date.getText().toString();
        String time = btn_add_med_time.getText().toString();
        Medicine medicine = new Medicine(name, category);
        Entry entry = new Entry(date, time, null, null, EntryCategories.MEDICINE.toString());
        if (isUpdateView) {
            entry.setId(getArguments().getInt(KeysForEntry.ID.toString()));
            medicine.setId(getArguments().getInt(KeysForMedicine.ID_MEDICINE.toString()));
            MedicineTook medicineTook = new MedicineTook(entry.getId(), medicine.getId(), dose);
            updateEntry(medicine, entry, medicineTook);
        } else {
            createNewEntry(medicine, entry, dose);
        }
        getActivity().finish();
    }

    private void updateEntry(final Medicine medicine, final Entry entry, final MedicineTook medicineTook) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .updateEntry(entry);
                ErnstDatabase.getInstance(getActivity())
                        .medicineDao()
                        .updateMedicine(medicine);
                ErnstDatabase.getInstance(getActivity())
                        .medicineTookDao()
                        .updateMedicineTook(medicineTook);
            }
        }).start();
    }

    private void createNewEntry(final Medicine medicine, final Entry entry, final String dose) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                long medicineId = ErnstDatabase.getInstance(getActivity())
                        .medicineDao()
                        .insertMedicine(medicine);
                long entryId = ErnstDatabase.getInstance(getActivity())
                        .entryDao()
                        .insertEntry(entry);
                MedicineTook medicineTook = new MedicineTook((int) entryId, (int) medicineId, dose);
                ErnstDatabase.getInstance(getActivity())
                        .medicineTookDao()
                        .insertMedicineTook(medicineTook);
            }
        }).start();
    }

}
