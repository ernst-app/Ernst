package de.zissithebee.ernst.fragments.tourFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.zissithebee.ernst.R;
import de.zissithebee.ernst.helper.ViewHandler;


public class TourRememberMeFragment extends Fragment {

    private View view;

    private ViewHandler viewHandler;

    private final int DURATION_TO_FADE_REMEMBER_ME_TEXT_IN = 4000;

    private TextView tvRemember, tvRememberMe;

    private List<TextView> tvs;


    public TourRememberMeFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tour_remember_me, container, false);
        viewHandler = new ViewHandler(getActivity());
        initializeTvs();
        fadeViewsIn();
        return view;
    }

    private void initializeTvs() {
        tvs = new ArrayList<>();
        tvRemember = (TextView) view.findViewById(R.id.welcome_remember);
        tvs.add(tvRemember);

        tvRememberMe = (TextView) view.findViewById(R.id.welcome_remember_me);
        tvs.add(tvRememberMe);
    }

    private void fadeViewsIn() {
        viewHandler.fadeTextIn(tvRememberMe, DURATION_TO_FADE_REMEMBER_ME_TEXT_IN);
    }

}
