package de.zissithebee.ernst.asynctasks;

import android.os.AsyncTask;

import de.zissithebee.ernst.database.Entry;
import de.zissithebee.ernst.database.ErnstDatabase;
import de.zissithebee.ernst.fragments.drawerFragments.DetailsFragment;

public class GetEntriesOnSpecialDay extends AsyncTask<DetailsFragment, Entry, Entry> {

    private DetailsFragment detailsFragment;
    private String date;

    public GetEntriesOnSpecialDay(String date) {
        this.date = date;
    }

    @Override
    protected Entry doInBackground(DetailsFragment... detailsFragments) {
        detailsFragment = detailsFragments[0];
        QueryHelper queryHelper = new QueryHelper(detailsFragment.getActivity());
        Entry[] entries = ErnstDatabase.getInstance(detailsFragment.getActivity())
                .entryDao()
                .getEntriesByDate(date);
        entries = queryHelper.getAllEntries(entries);
        publishProgress(entries);
        return null;
    }

    @Override
    protected void onProgressUpdate(Entry... entries) {
        detailsFragment.updateView(entries);
        super.onProgressUpdate(entries);
    }
}


